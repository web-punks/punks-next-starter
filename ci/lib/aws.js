const run = require("./run").run

exports.ecrLogin = () => {
  run(
    `aws ecr get-login-password --region ${process.env.AWS_REGION} | docker login --username ${process.env.AWS_ECR_USERNAME} --password-stdin ${process.env.AWS_ECR_REGISTRY}`
  )
}
