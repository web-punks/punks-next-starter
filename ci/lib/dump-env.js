const fs = require("fs")
const path = require("path")

const parseVariables = (content) => {
  const map = {}
  content
    .split("\n")
    .filter((x) => x.trim() !== "" && x.indexOf("=") > 0)
    .forEach((x) => (map[x.split("=")[0]] = x.split("=")[1]))
  return map
}

const extractVariables = (prefix) => {
  const variables = {}
  Object.keys(process.env)
    .filter((x) => x.toUpperCase().startsWith(prefix.toUpperCase()))
    .forEach(
      (key) => (variables[key.substring(prefix.length)] = process.env[key])
    )
  return variables
}

const mergeVariables = (variables, other) => {
  const copy = JSON.parse(JSON.stringify(variables))
  Object.keys(other).forEach((key) => {
    if (copy[key] !== undefined) {
      copy[key] = other[key]
    }
  })
  return copy
}

const stringifyVariables = (variables) =>
  Object.keys(variables)
    .map((key) => `${key}=${variables[key]}`)
    .join("\n")

const stringifyPythonVariables = (variables) =>
  Object.keys(variables)
    .map((key) => `${key}="${variables[key]}"`)
    .join("\n")

const exportVariables = (variables, format) => {
  return format === "py"
    ? stringifyPythonVariables(variables)
    : stringifyVariables(variables)
}

const getAllPrefixes = (prefixes) => ["", ...prefixes]

exports.dumpEnv = (params) => {
  let variables = parseVariables(
    fs.readFileSync(path.join(params.folder, params.sourceEnv), "utf-8")
  )

  for (const prefix of getAllPrefixes(params.prefixes || [])) {
    const processVars = extractVariables(prefix)
    variables = mergeVariables(variables, processVars)
  }

  fs.writeFileSync(
    path.join(params.folder, params.targetEnv),
    exportVariables(variables, params.targetEnv.split(".").pop())
  )
}
