const run = require("./run").run
const yaml = require("js-yaml")

const dcBaseCommand = (environment) =>
  `docker-compose -f docker-compose.yml -f docker/docker-compose.ci.yml ${
    environment ? `-f docker/docker-compose.${environment}.yml` : ""
  }`

exports.dc = (params) =>
  run(
    `${dcBaseCommand(params.environment)} ${params.cmd} ${params.service} ${
      params.args ? `${params.args}` : ""
    }`,
    params.whatIf
  )

const dcEnvConfig = (environment) => {
  const raw = run(`${dcBaseCommand(environment)} config`)
  return yaml.safeLoad(raw)
}

exports.dcSvcConfig = (environment, service) =>
  dcEnvConfig(environment).services[service]

exports.tagImage = (source, target) => run(`docker tag ${source} ${target}`)
exports.pushImage = (image) => run(`docker push ${image}`)
exports.lsImages = () => run(`docker image ls`)
exports.getImageTag = (image) => {
  const parts = image.split(":")
  return parts[parts.length - 1]
}
