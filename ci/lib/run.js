const sh = require("shelljs")
sh.config.fatal = true
sh.config.verbose = true

exports.run = (command, whatIf = false) => {
  if (whatIf) {
    console.log(`CMD -> ${command}`)
    return
  }
  const result = sh.exec(command)
  if (result.code !== 0) {
    throw new Error(
      `Error executing command ${command} -> code ${result.code}: ${result.stderr}`
    )
  }
  return result.stdout
}
