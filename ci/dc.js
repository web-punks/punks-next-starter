#!/usr/bin/env node

require("dotenv").config()

const yargs = require("yargs/yargs")
const dumpEnv = require("./lib/dump-env").dumpEnv
const docker = require("./lib/docker")
const aws = require("./lib/aws")
const run = require("./lib/run").run

console.log("ss", process.env.AWS_REGION)

const argv = yargs(process.argv.slice(2)).options({
  service: { type: "string", alias: "svc", demandOption: true },
  environment: { type: "string", alias: "env", demandOption: true },
  build: { type: "boolean" },
  pull: { type: "boolean" },
  push: { type: "string" },
  run: { type: "string" },
  dumpEnv: { type: "string", alias: "de" },
  dumpEnvSource: { type: "string", alias: "deSrc" },
  dumpEnvTarget: { type: "string", alias: "deTarget" },
  copy: { type: "string", alias: "cp" },
  commitHash: {
    type: "string",
    default: process.env.CI_COMMIT_SHA || run("git rev-parse HEAD"),
  },
  whatIf: { type: "boolean" },
}).argv

if (argv.dumpEnv) {
  dumpEnv({
    folder: argv.dumpEnv,
    sourceEnv: argv.dumpEnvSource ? argv.dumpEnvSource : ".env.template",
    targetEnv: argv.dumpEnvTarget ? argv.dumpEnvTarget : ".env.production",
    prefixes: [
      `${argv.environment}_`,
      `${argv.service}_`,
      `${argv.environment}_${argv.service}_`,
    ],
  })
}

console.log(`Commit hash ${argv.commitHash}`)

if (argv.pull || argv.build) {
  docker.dc({
    cmd: "pull",
    environment: argv.environment,
    service: argv.service,
    whatIf: argv.whatIf,
  })
}

if (argv.build) {
  docker.dc({
    cmd: "build",
    environment: argv.environment,
    service: argv.service,
    whatIf: argv.whatIf,
  })
  docker.dc({
    cmd: "push",
    environment: argv.environment,
    service: argv.service,
    whatIf: argv.whatIf,
  })
}

if (argv.run) {
  const commands = Array.isArray(argv.run) ? argv.run : [argv.run]
  for (const command of commands) {
    docker.dc({
      cmd: "run --rm",
      environment: argv.environment,
      service: argv.service,
      args: command.trim(),
      whatIf: argv.whatIf,
    })
  }
}

if (argv.copy) {
  const pathsList = Array.isArray(argv.copy) ? argv.copy : [argv.copy]
  for (const paths of pathsList) {
    docker.dc({
      cmd: "run --rm",
      environment: argv.environment,
      service: argv.service,
      args: `cp -r ${paths.split(" ")[0]} ${paths.split(" ")[1]}`,
      whatIf: argv.whatIf,
    })
  }
}

if (argv.push) {
  if (!argv.commitHash) {
    throw new Error("Cannot find CI_COMMIT_SHA env variable")
  }
  aws.ecrLogin()
  const imageName = docker.dcSvcConfig(argv.environment, argv.service).image
  console.log("Source image ->", imageName)
  const ecrImageName = `${process.env.AWS_ECR_REGISTRY}/${
    argv.push
  }:${docker.getImageTag(imageName)}-${argv.commitHash}`
  docker.tagImage(imageName, ecrImageName)
  docker.pushImage(ecrImageName)
}
