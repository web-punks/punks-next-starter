export const DEFAULT_PREVIEW_URL = "http://localhost:3033/preview"

// TODO: put token in env vars
const readToken = "xxxxx"

const DEFAULT_PROJECT_ID = "srnt2ltl"
const DEFAULT_DATASET = "production"

export default function resolveProductionUrl(document: any) {
  return `${process.env.PREVIEW_BASE_URL ?? DEFAULT_PREVIEW_URL}?id=${
    document._id
  }&rev=${
    document._rev
  }&token=${readToken}&projectId=${DEFAULT_PROJECT_ID}&dataset=${DEFAULT_DATASET}`
}
