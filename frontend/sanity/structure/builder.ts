import * as _ from "lodash"
import { excludedSchemas } from "./sections"

const splitSchemasByCategory = (items: any[]) =>
  _.groupBy(items, (x) => x.spec.schemaType?.category ?? "default")

const filterExcludedSchemas = (items: any[]) =>
  items.filter((x) => !excludedSchemas.includes(x.spec.id))

export const getSchemasSections = (items: any[]) =>
  splitSchemasByCategory(filterExcludedSchemas(items))
