import { MdStyle, MdSettings } from "react-icons/md"
import { FaStore } from "react-icons/fa"

export const rootSections = [
  {
    id: "default",
    title: "Default",
    icon: MdSettings,
  },
  {
    id: "website",
    title: "Website",
    icon: FaStore,
  },
]

export const excludedSchemas = []
