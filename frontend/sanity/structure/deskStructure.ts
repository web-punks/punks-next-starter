import * as Structure from "sanity-plugin-intl-input/lib/structure"
import S from "@sanity/desk-tool/structure-builder"
import { getSchemasSections } from "./builder"
import { rootSections } from "./sections"

// default implementation by re-exporting
export const getDefaultDocumentNode = Structure.getDefaultDocumentNode
// export default Structure.default

// // or manual implementation to use with your own custom desk structure
// export const getDefaultDocumentNode = (props) => {
//   if (props.schemaType === "myschema") {
//     return S.document().views(
//       Structure.getDocumentNodeViewsForSchemaType(props.schemaType)
//     )
//   }
//   return S.document()
// }

const groupedItems = getSchemasSections(
  Structure.getFilteredDocumentTypeListItems()
)

export default () => {
  return S.list()
    .id("__root__")
    .title("Content")
    .items(
      rootSections.map((section) =>
        S.listItem()
          .title(section.title)
          .icon(section.icon)
          .child(S.list().title(section.title).items(groupedItems[section.id]))
      )
    )
}
