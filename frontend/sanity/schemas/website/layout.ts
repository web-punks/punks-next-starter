import Tabs from "sanity-plugin-tabs"

const layout = {
  name: "websiteLayout",
  type: "document",
  title: "Layout",
  i18n: true,
  category: "website",
  fields: [
    {
      name: "data",
      type: "layoutContents",
    },
  ],
  preview: {
    select: {
      title: "data.info.name",
    },
  },
}

const layoutContents = {
  title: "Layout Content",
  name: "layoutContents",
  type: "object",
  inputComponent: Tabs,
  fieldsets: [
    { name: "info", title: "Info" },
    { name: "header", title: "Header" },
    { name: "footer", title: "Footer" },
    { name: "body", title: "Body" },
  ],
  options: {
    layout: "object",
  },
  fields: [
    {
      name: "info",
      type: "layoutInfo",
      title: "Info",
      fieldset: "info",
    },
    {
      name: "header",
      type: "headerContents",
      title: "Header",
      fieldset: "header",
    },
    {
      name: "footer",
      type: "footerContents",
      title: "Footer",
      fieldset: "footer",
    },
    {
      name: "body",
      type: "bodyContents",
      title: "Body",
      fieldset: "body",
    },
  ],
}

const info = {
  title: "Info",
  name: "layoutInfo",
  type: "object",
  fields: [
    {
      name: "name",
      type: "string",
    },
  ],
}

const header = {
  title: "Header",
  name: "headerContents",
  type: "object",
  fields: [
    {
      name: "logo",
      type: "image",
    },
    {
      name: "menu",
      type: "array",
      of: [
        {
          type: "reference",
          to: { type: "navigationMenu" },
        },
      ],
    },
  ],
}

const footer = {
  title: "Footer",
  name: "footerContents",
  type: "object",
  fields: [
    {
      name: "companyInfo",
      type: "richText",
    },
  ],
}

const body = {
  title: "Body",
  name: "bodyContents",
  type: "object",
  fields: [
    {
      name: "background",
      type: "layoutBackground",
    },
  ],
}

const background = {
  title: "Background",
  name: "layoutBackground",
  type: "object",
  fields: [
    {
      name: "image",
      type: "image",
    },
    {
      name: "texture",
      type: "boolean",
    },
  ],
}

export const layoutTypes = [
  layout,
  layoutContents,
  info,
  header,
  footer,
  background,
  body,
]
