import Tabs from "sanity-plugin-tabs"

const page = {
  name: "page",
  type: "document",
  title: "Page",
  i18n: true,
  category: "website",
  fields: [
    {
      name: "data",
      type: "pageContent",
    },
  ],
  preview: {
    select: {
      title: "data.seo.title",
    },
  },
}

const pageContent = {
  title: "Page Content",
  name: "pageContent",
  type: "object",
  inputComponent: Tabs,
  fieldsets: [
    { name: "meta", title: "SEO" },
    { name: "main", title: "Main" },
  ],
  options: {
    layout: "object",
  },
  fields: [
    {
      name: "seo",
      type: "seo",
      title: "SEO Metadata",
      fieldset: "meta",
    },
    {
      name: "body",
      type: "richText",
      fieldset: "main",
    },
    {
      name: "image",
      type: "image",
      fieldset: "main",
    },
    {
      name: "imageLink",
      title: "Image link",
      type: "linkField",
      fieldset: "main",
    },
    {
      name: "imageSubtitle",
      type: "string",
      fieldset: "main",
    },
    {
      name: "imageSubtitleLink",
      title: "Image Subtitle link",
      type: "linkField",
      fieldset: "main",
    },
  ],
}

export const pageTypes = [page, pageContent]
