import Tabs from "sanity-plugin-tabs"

const menu = {
  name: "navigationMenu",
  type: "document",
  title: "Menu",
  i18n: true,
  category: "website",
  fields: [
    {
      name: "data",
      type: "navigationMenuContents",
    },
  ],
  preview: {
    select: {
      title: "data.name",
    },
  },
}

const menuContents = {
  title: "Menu Content",
  name: "navigationMenuContents",
  type: "object",
  inputComponent: Tabs,
  fieldsets: [
    { name: "info", title: "Info" },
    { name: "items", title: "Items" },
  ],
  options: {
    layout: "object",
  },
  fields: [
    {
      fieldset: "info",
      name: "name",
      type: "string",
      title: "Name",
    },
    {
      fieldset: "items",
      name: "items",
      title: "Items",
      type: "array",
      of: [
        {
          type: "navigationMenuItem",
        },
      ],
    },
  ],
}

const menuItem = {
  title: "Menu Item",
  name: "navigationMenuItem",
  type: "object",
  fields: [
    {
      name: "label",
      type: "string",
      title: "Label",
    },
    {
      name: "target",
      title: "Target",
      type: "reference",
      to: [{ type: "page" }],
    },
    {
      name: "children",
      title: "Subitems",
      type: "array",
      of: [
        {
          type: "navigationMenuItem",
        },
      ],
    },
  ],
}

export const menuTypes = [menu, menuContents, menuItem]
