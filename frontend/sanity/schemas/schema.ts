import createSchema from "part:@sanity/base/schema-creator"

import schemaTypes from "all:part:@sanity/base/schema-type"
import { pageTypes } from "./website/page"
import { layoutTypes } from "./website/layout"
import { menuTypes } from "./website/menu"
import { commonTypes } from "sanity-plugin-punks"

const common = commonTypes({
  links: {
    pageTypes: ["page"],
  },
})

export default createSchema({
  name: "default",
  types: schemaTypes.concat([
    ...common,
    ...pageTypes,
    ...layoutTypes,
    ...menuTypes,
  ]),
})
