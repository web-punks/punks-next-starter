import React from "react"
import { storiesOf } from "@storybook/react"
import { withInfo } from "@storybook/addon-info"
import { CustomTypography } from "@punks/ui-mui"
import AppContextDecorator from "../testing/decorators/AppContextDecorator"

const story = storiesOf("Theme/Typography", module)
  .addDecorator(withInfo)
  .addDecorator(AppContextDecorator)

const Fonts = () => (
  <>
    <CustomTypography variant="h1">Title 1</CustomTypography>
    <CustomTypography variant="h2">Title 2</CustomTypography>
    <CustomTypography variant="h3">Title 3</CustomTypography>
    <CustomTypography variant="h4">Title 4</CustomTypography>
    <CustomTypography variant="h5">Title 5</CustomTypography>
    <CustomTypography variant="h6">Title 6</CustomTypography>
    <CustomTypography variant="subtitle1">Subtitle 1</CustomTypography>
    <CustomTypography variant="subtitle2">Subtitle 2</CustomTypography>
    <CustomTypography variant="body1">Body 1</CustomTypography>
    <CustomTypography variant="body2">Body 2</CustomTypography>
    <CustomTypography variant="overline">Overline</CustomTypography>
    <CustomTypography variant="caption" component="div">
      Caption
    </CustomTypography>
  </>
)

story.add("Fonts", () => <Fonts />)

// story.add("Fonts Mobile", () => (
//   <>
//     <Fonts />
//   </>
// ))
