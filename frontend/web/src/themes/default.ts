import { AppTheme } from "@punks/core"

export const defaultAppTheme: AppTheme = {
  name: "default",
  default: true,
  background: {},
  breakpoints: {
    mobileBreakpoint: "sm",
  },
  font: {
    styles: {
      defaults: {
        fontFamily: "gill sans nova",
      },
      textColors: {
        primary: "#000",
      },
    },
    typography: {
      body1: {
        size: "1rem",
      },
      body2: {
        size: "0.875rem",
      },
      h1: {
        size: "6.25rem",
      },
      h2: {
        size: "4.75rem",
      },
      h3: {
        size: "3.125rem",
      },
      h4: {
        size: "1.875rem",
      },
      h5: {
        size: "1.5625rem",
      },
      h6: {
        size: "1.375rem",
      },
      overline: {
        size: "1.125rem",
      },
      subtitle1: {
        size: "1.25rem",
      },
      subtitle2: {
        size: "1.125rem",
      },
    },
  },
  styles: {
    borders: {
      borderRadius: {},
    },
    shadows: {
      disabled: true,
    },
  },
  palette: {
    action: {
      selected: "rgba(0, 0, 0, 0.08)",
      disabled: "#f2f2f2",
      active: "#d6d6d6",
    },
    primary: {
      light: "#ffffca",
      main: "#fddf99",
      dark: "#dbbc71",
    },
    secondary: {
      light: "#666665",
      main: "#3c3c3b",
      dark: "#161615",
    },
    error: {
      light: "#e85653",
      main: "#b0202a",
      dark: "#790000",
    },
    success: {
      main: "#f4f2eb",
    },
  },
  components: {
    button: {
      contained: {},
      default: {},
      outlined: {},
    },
  },
}
