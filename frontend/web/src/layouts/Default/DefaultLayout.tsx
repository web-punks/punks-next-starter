import React from "react"
import { makeStyles } from "@material-ui/core"

export interface DefaultLayoutProps {
  children: any
}

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
  },
  content: {
    flex: 1,
  },
}))

export const DefaultLayout = ({ children }: DefaultLayoutProps) => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <div>{/* Header */}</div>
      <div className={classes.content}>{children}</div>
      <div>{/* Footer */}</div>
    </div>
  )
}
