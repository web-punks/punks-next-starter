import React from "react"
import {
  isPreviewSession,
  fetchDocuments,
  PageInfo,
} from "@punks/contents-sanity"
import { usePreviewSubscription } from "../root/sanity/client"
import { pageQuery, pathsQuery } from "../contents/page"
import { buildNextPath } from "../root/routing"
import { LayoutContentsProvider } from "../contents/layout/context"
import { DefaultLayout } from "../layouts"
import { RichTextField } from "@punks/core"
import { extractPageInfo } from "../root/routing/locales"

interface Props {
  contents: any
  layoutData: any
  path: PageInfo
}

export default function LandingPage({ contents, layoutData, path }: Props) {
  const { data } = usePreviewSubscription(pageQuery, {
    params: {
      ...path,
    },
    initialData: contents,
    enabled: isPreviewSession(),
  })

  return (
    <LayoutContentsProvider value={layoutData}>
      <DefaultLayout>
        <RichTextField value={data.data.body} />
      </DefaultLayout>
    </LayoutContentsProvider>
  )
}

export async function getStaticProps({ params }: any) {
  const path = extractPageInfo(params.path)
  const contents = await fetchDocuments(pageQuery, path)
  return {
    props: {
      contents,
      path,
    },
  }
}

export async function getStaticPaths() {
  const pages = await fetchDocuments(pathsQuery)
  return {
    paths:
      pages?.map((page: any) => ({
        params: {
          path: buildNextPath(page),
        },
      })) ?? [],
    fallback: false,
  }
}
