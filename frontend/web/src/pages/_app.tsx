import React, { FC } from "react"
import { AppProps } from "next/app"
import { WrapRoot } from "../root/index"

const CustomApp: FC<AppProps> = ({ Component, pageProps }) => {
  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side")
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles)
    }
  }, [])

  return (
    <WrapRoot>
      <Component {...pageProps} />
    </WrapRoot>
  )
}

export default CustomApp
