import React from "react"
import { PreviewLoader } from "@punks/contents-sanity"

const PreviewPage = () => {
  return <PreviewLoader />
}

export default PreviewPage
