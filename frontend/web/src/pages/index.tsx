import React from "react"
import { isPreviewSession } from "@punks/contents-sanity"
import { getClient, usePreviewSubscription } from "../root/sanity/client"
import { DefaultLayout } from "../layouts"
import { fetchLayoutData } from "../contents/layout"
import { LayoutContentsProvider } from "../contents/layout/context"
import { homePageQuery } from "../contents/page"
import { RichTextField } from "@punks/core"

interface Props {
  contents: any
  layoutData: any
  preview: boolean
}

export default function HomePage({ contents, layoutData }: Props) {
  const { data } = usePreviewSubscription(homePageQuery, {
    params: { locale: "it_it" },
    initialData: contents,
    enabled: isPreviewSession(),
  })
  return (
    <LayoutContentsProvider value={layoutData}>
      <DefaultLayout>
        <RichTextField value={data.data.body} />
      </DefaultLayout>
    </LayoutContentsProvider>
  )
}

export async function getStaticProps() {
  const contents = await getClient().fetch(homePageQuery, {
    locale: "it_it",
  })
  const layoutData = await fetchLayoutData("it_it")
  return {
    props: {
      contents,
      layoutData,
    },
  }
}
