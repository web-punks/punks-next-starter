import { localizePath } from "@punks/contents-sanity"
import { locales } from "./locales"

const pagePath = (document: any) =>
  document?.data?.seo?.slug ? `/${document?.data?.seo?.slug}` : "/"

const pathPrefixes = new Map<string, string>([])

const getPagePrefix = (type: string) =>
  pathPrefixes.has(type) ? pathPrefixes.get(type) : ""

const getNonLocalizedPath = (document: any) => {
  switch (document._type) {
    case "page":
      return pagePath(document)
  }
  console.log("cannot resolve url for", document)
  return ""
}

export const urlBuilder = (document: any) =>
  localizePath(document, getNonLocalizedPath(document), locales)

export const buildNextPath = (document: any) =>
  urlBuilder(document).substring(1).split("/")

export const buildPagePath = (document: any) =>
  localizePath(
    document,
    `${getPagePrefix(document._type)}${getNonLocalizedPath(document)}`,
    locales
  )
