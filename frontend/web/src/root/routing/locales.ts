import { LocaleSettings, processPath } from "@punks/contents-sanity"

export const locales: LocaleSettings[] = [
  {
    id: "it_it",
    path: "",
  },
  {
    id: "en_us",
    path: "en",
  },
]

export const extractPageInfo = (path: string[]) => processPath(path, locales)
