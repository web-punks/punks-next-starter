import React from "react"
import { MuiProviders } from "@punks/ui-mui"
import { NextProviders } from "@punks/next"
import SanityPunksProviders from "./sanity"
import CoreProviders from "./core"
import { AppThemesContext } from "@punks/core"
import { defaultAppTheme } from "../../themes/default"

interface Props {
  children: any
}

export const PunksProviders = ({ children }: Props) => {
  return (
    <CoreProviders>
      <NextProviders>
        <SanityPunksProviders>
          <AppThemesContext.Provider value={[defaultAppTheme]}>
            <MuiProviders>{children}</MuiProviders>
          </AppThemesContext.Provider>
        </SanityPunksProviders>
      </NextProviders>
    </CoreProviders>
  )
}
