import React from "react"
import { RootProviders } from "@punks/core"
import { urlBuilder } from "../../routing"

interface Props {
  children: any
}

const CoreProviders = ({ children }: Props) => {
  return (
    <RootProviders
      urlBuilder={urlBuilder}
      metadata={{
        description: "Punx Starter",
        title: "Punx Starter",
      }}
    >
      {children}
    </RootProviders>
  )
}

export default CoreProviders
