import React from "react"
import { SanityWebsiteThemeProvider } from "@punks/contents-sanity"

export const SiteThemeProvider = ({ children }: any) => {
  return (
    <SanityWebsiteThemeProvider
      value={{
        allSanityWebsiteTheme: {
          nodes: [
            {
              data: {
                main: {
                  isDefault: true,
                  name: "default",
                },
                breakpoints: {
                  mobileBreakpoint: "sm",
                },
                backgrounds: null,
                components: null,
                font: null,
                palette: null,
                styles: null,
              },
            },
          ],
        },
      }}
    >
      {children}
    </SanityWebsiteThemeProvider>
  )
}
