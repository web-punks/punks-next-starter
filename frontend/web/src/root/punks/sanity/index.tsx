import React from "react"
import { SanityProviders } from "@punks/contents-sanity"
import { SiteThemeProvider } from "./SiteThemeProvider"

interface Props {
  children: any
}

const SanityPunksProviders = ({ children }: Props) => {
  return (
    <SanityProviders
      linkProcessorType="dynamic"
      clientConfig={{
        dataset: process.env.SANITY_DATASET ?? "",
        projectId: process.env.SANITY_PROJECT_ID ?? "",
      }}
    >
      <SiteThemeProvider>{children}</SiteThemeProvider>
    </SanityProviders>
  )
}

export default SanityPunksProviders
