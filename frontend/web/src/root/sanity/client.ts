import sanityClient from "@sanity/client"
import { createPreviewSubscriptionHook } from "next-sanity"

export const client = () =>
  sanityClient({
    dataset: process.env.SANITY_DATASET,
    projectId: process.env.SANITY_PROJECT_ID ?? "",
    useCdn: process.env.NODE_ENV === "production",
  })

export const previewClient = () =>
  sanityClient({
    dataset: process.env.SANITY_DATASET,
    projectId: process.env.SANITY_PROJECT_ID ?? "",
    token: process.env.SANITY_TOKEN,
    useCdn: false,
  })

export const usePreviewSubscription = createPreviewSubscriptionHook({
  dataset: process.env.SANITY_DATASET ?? "",
  projectId: process.env.SANITY_PROJECT_ID ?? "",
})

export const getClient = (preview = false) =>
  preview ? previewClient() : client()
