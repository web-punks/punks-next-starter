import React from "react"
import { Provider } from "react-redux"
import store from "../state/store"
import { PunksProviders } from "./punks"

interface Params {
  children: any
}

export const WrapRoot = ({ children }: Params) => {
  return (
    <PunksProviders>
      <Provider store={store}>{children}</Provider>
    </PunksProviders>
  )
}
