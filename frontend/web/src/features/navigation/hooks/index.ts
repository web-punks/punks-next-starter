import { useAppSelector } from "../../../state/store"

export const useIsFirstPage = () =>
  useAppSelector((state) => state.navigation).isFirstPage
