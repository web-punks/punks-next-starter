import groq from "groq"

export const homePageQuery = groq`*[_type == "page" && __i18n_lang == $locale && (!defined(data.seo.slug) || data.seo.slug == '')][0] {
  ...
}`

export const pathsQuery = groq`*[_type == "page" && defined(data.seo.slug) && data.seo.slug != ''] {
  _id,
  _type,
  __i18n_lang,
  data {
    seo {
      slug
    }
  }
}`

export const pageQuery = groq`*[_type == "page" && __i18n_lang == $locale && data.seo.slug == $slug][0]`
