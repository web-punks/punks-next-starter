import groq from "groq"

export const layoutDataQuery = groq`
*[_type == "websiteLayout"][0] {
    ...,
    "ref": {
        "menuPanels": data.header.menu[]->
      }
  }
`
