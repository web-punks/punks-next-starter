import React from "react"

export type LayoutContents = any

export const LayoutContentsContext = React.createContext<LayoutContents>({})

export const LayoutContentsProvider = LayoutContentsContext.Provider

export const useLayoutContents = () => React.useContext(LayoutContentsContext)
