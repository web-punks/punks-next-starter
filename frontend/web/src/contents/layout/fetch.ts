import { fetchDocuments } from "@punks/contents-sanity"
import { layoutDataQuery } from "./queries"

export const fetchLayoutData = async (language: string) =>
  fetchDocuments(layoutDataQuery, {
    lang: language,
  })
