import { combineReducers } from "@reduxjs/toolkit"

import navigationReducer from "../features/navigation/state/navigationSlice"

const rootReducer = combineReducers({
  navigation: navigationReducer,
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
