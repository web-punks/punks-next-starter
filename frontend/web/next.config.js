const getEnvVars = () => {
  const {
    NODE_ENV,
    NODE_OPTIONS,
    NODE_VERSION,
    __NEXT_PROCESSED_ENV,
    ...other
  } = process.env // eslint-disable-line
  return other
}

module.exports = {
  webpackDevMiddleware: (config) => {
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    }

    return config
  },
  env: {
    ...getEnvVars(),
  },
  trailingSlash: true,
  productionBrowserSourceMaps: true,
}
